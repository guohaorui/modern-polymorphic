#include <iostream>
#include "square.h"
#include "rectangle.h"
#include "shape.h"
using namespace std;

void draw_shape(Shape *s)
{
    s->draw();
}

// Shape -> Rectangle -> Square
// draw()
int main(int argc, char *argv[])
{
    // 静态绑定的不足
    // Square sq1(3.0, "Square1");
    Shape *p = new Square(1.0, "Square1");
    delete p;

    cout << "----- yz ------" << endl;
    return 0;
}