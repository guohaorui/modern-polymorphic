#include <iostream>
#include "square.h"
#include "rectangle.h"
#include "shape.h"
using namespace std;

void draw_shape(Shape *s)
{
    s->draw();
}

// Shape -> Rectangle -> Square
// draw()
int main(int argc, char *argv[])
{
    // 静态绑定的不足
    Shape s1("Shape1");
    Rectangle r1(1.0, 2.0, "Rectangle1");
    Square sq1(3.0, "Square1");
    cout << "--------------" << endl;
    // Base pointers
    Shape *shape_ptr = &s1;
    draw_shape(shape_ptr);
    shape_ptr = &r1;
    draw_shape(shape_ptr);
    shape_ptr = &sq1;
    draw_shape(shape_ptr);
    cout << "-------------" << endl;
    // collections
    Shape shapes[]{s1, r1, sq1};
    for (Shape &s : shapes)
    {
        s.draw();
    }

    // Shape ref
    // Shape &shape_ref[]{s1, r1, sq1};
    cout << "point" << endl;
    // pointer
    Shape *shapes_ptr[]{&s1, &r1, &sq1};
    for (Shape *s : shapes_ptr)
    {
        s->draw();
    }

    cout << "----- yz ------" << endl;
    return 0;
}